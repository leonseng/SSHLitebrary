# SSHLiteLibrary

## Introduction

SSHLiteLibrary is a cut down version of the Robot Framework SSHLibrary. It provides SSHLibrary (v3.1.0) functions without the need to install Robot Framework.

## Documentation

See <http://robotframework.org/SSHLibrary/SSHLibrary.html>